1) DESCRIPTION

PUReIBM-FFD is a three-dimensional fully finite-difference particle-resolved
direct numerical simulation solver for detailed analysis of flow and heat and mass transfer. PUReIBM-FFD
is a continuum Navier-Stokes and scalar solvers based on Cartesian grid that utilizes
Immeresed Boundary method to represent particle surfuces. The details about the solvers
can be found in the papers from SUBRAMANIAM's group. 

2) REQUIRED PACKAGES

In order to use PUReIBM-FFD codes, PETSc software (http://www.mcs.anl.gov/petsc/) needs to be installed in your machines. 
Please see http://www.mcs.anl.gov/petsc/ to learn how to install petsc in windows and macs. 
Below is only showing how to install PETSc in linux.

Warning : Since the PETSc has many different versions,  the commands varies with versions (most are the same.) 
In the PUReIBM-FFD codes,  petsc-3.4.3 is used. You can find this version of PETSc under PETSc folder.
Details about the changes for this version of PETSc can be found in http://www.mcs.anl.gov/petsc/documentation/changes/34.html.

PUReIBM-FFD codes have been tested and run in parallel in the different clusters as Cyence, Condo in ISU, Comet and Stampede in Xsede, 
Blue water in NCSA.


The parallelization of PUReIBM-FFD is 3D domain decomposition technique in the physical space. 


3) COMPILATION

The compilation process can be done as the follow setups:

  1. To extract  PETSc sources code use: 

    $ gunzip -c petsc-3.4.3.tar.gz | tar -xof -

  2. Go to the folder "petsc-3.4.3"

  3. Configure PETsc : The command line for Configure is different from machines. 

     $ ./configure 
       (see http://www.mcs.anl.gov/petsc/petsc-as/documentation/installation.html for details, for example 
       ./configure --configModules=PETSc.Configure --optionsModule=PETSc.compilerOptions --with-mpi-dir=/shared/openmpi/gcc/bin 
       --download-f-blas-lapack=1 --FFLAGS=-I/shared/openmpi/gcc/include --with-mpiexec=mpirun --with-debugging=0 
       COPTFLAGS='-O3 -march=p4 -mtune=p4' FOPTFLAGS='-O3 -qarch=p4 -qtune=p4' )

    Then

    $ make PETSC_DIR=/work/mech_eng3/sciam/petsc-3.4.3 PETSC_ARCH=arch-linux2-c-debug all
    
   If you have any doubt about which switch you should use, please ask the administer of the cluster for help.

  4. Go to the source file folder to compile your codes using makefile
  
    $ make main
    
    Then the executable file is generated as "main"

  5. Run a test case in a script 
  
   For instance, if the input file name is "test_floparam.in" (This file is included in main folder),
   then you can submit your case into a cluster (if it uses PBS) using the following script:
  
	#!/bin/tcsh

	#PBS  -o BATCH_OUTPUT
	#PBS  -e BATCH_ERRORS

	#PBS -lnodes=8:ppn=16:compute,walltime=24:00:00

	# Change to directory from which qsub command was issued
	cd $PBS_O_WORKDIR

		mpirun -np 128 ./main test > print_test &
		wait

(4) Acknowledgement      
    For acknowledgement, please refer to the following publications:
  
     (1) TENNETI, S. and SUBRAMANIAM, S., 2014, Particle-resolved direct numerical
        simulation for gas-solid flow model development. Annu. Rev. Fluid Mech.
        46 (1) 199-230.
     (2) B. Sun,2015, Modeling Heat and Mass Transfer in Reacting Gas-solid Flow 
         Using Particle-resolved Direct Numerical Simulation. PhD thesis, Iowa State University.