!    PUReIBM-FFD is a three-dimensional fully finite-differnce particle-resolved
!    direct numerical simulation solver for detailed analysis of flow and heat and mass transfer problem. PUReIBM-FFD
!    is a continuum Navier-Stokes and scalar solvers based on Cartesian grid that utilizes
!    Immeresed Boundary method to represent particle surfuces. The details about the solvers
!    can be found in the below papers in SUBRAMANIAM's group. 
!    Copyright (C) 2015, Shankar Subramaniam, Rahul Garg, Sudheer Tenneti, Bo Sun, Mohammad Mehrabadi
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!    For acknowledgement, please refer to the following publications:
!   
!     (1) TENNETI, S. and SUBRAMANIAM, S., 2014, Particle-resolved direct numerical
!         simulation for gas-solid flow model development. Annu. Rev. Fluid Mech.
!         46 (1) 199-230.
!     (2) B. Sun,2015, Modeling Heat and Mass Transfer in Reacting Gas-solid Flow 
!         Using Particle-resolved Direct Numerical Simulation. PhD thesis, Iowa State University.
Module nlcalc 
#include "ibm.h"
  USE precision 
  USE constants 
  Use nl_allflow
  Use boundary_condition
  Use dependent_functions
  Use collision_mod
  USe machine
  USE parallel_solver
  use global_data
  implicit none 
  !-----------------------------------------------------------------------
  !	calculate the nonlinear (convective) terms, pressure gradient
  ! , and particle ib forcing in the NS equations
  !	the velocities 
  !
  !	
  !-----------------------------------------------------------------------
contains   

  subroutine nonlinear(rks,user_x,user_y,user_z)

    implicit none 
#include "include_petsc.h"

    type (userctx) user_x,user_y,user_z
    integer, Intent(in) ::  rks
    REAL(prcn) :: nlstart_time, nlend_time, temptime, global_max_nl_time
    !REAL(prcn), SAVE :: avg_nl_time = 0.d0, global_avg_nl_time = 0.d0
    REAL(prcn) :: avg_nl_time , global_avg_nl_time 
    INTEGER, SAVE :: itercount = 0
    !-----------------------------------------------------------------------
    !	local variables

    real(prcn) ::  eold, tvis, nlmean(ndim), nlmean_loc(ndim),brmax ,brmax_loc

    integer i,j,k,l,ii,n, unit1, jj,kk,imax,jmax,kmax
    REAL*8 :: utemp, dt_fluid_orig

    
    !--------------dt dont appear in vis coefficent----------------------------------------------
        cdiff = vis/dx/dx  !! for matrix
        cdiff_2 = cdiff/2.0d0
       
    if(myid.eq.0) write(*,*)'coe_for_matrix, cdiff,cdiff_2,cconv=',cdiff,cdiff_2,cconv

    avg_nl_time = zero

   if(myid.eq.0)  write(*,*)'rank',myid,'Beginning of nonlinear check ur_nlr_pr= ', ur(nxp/2, nyp/2,nzp/2,1:3),nlr(nxp/2,nyp/2,nzp/2,1:3),pr(nxp/2,nyp/2,nzp/2)

    urold = ur

  ! write(*,*)myid,'ur',ur(:,:,:,3)
    !! check
             cpos   = dx/(dt)
             brmax=zero
#if 0     
	do k=1,nzp
	  
	      kk=k+1
	  
	   do j=1,nyp
	      
	         jj=j+1
	   
	      do i=1,nxp
	         
	         ii=i+1

	            !! // can be changed see some paper
                 br(i,j,k,1)=(ur(ii,j,k,1)-ur(i,j,k,1)+ &
		                      ur(i,jj,k,2)-ur(i,j,k,2)+ &
	                          ur(i,j,kk,3)-ur(i,j,k,3))*cpos
                if(brmax.le.br(i,j,k,1)) then
                  brmax=max(br(i,j,k,1),brmax)
                  imax=i
                  jmax=j
                  kmax=k
                endif
	      enddo
	    enddo
	enddo

      GLOBAL_DOUBLE_MAX(brmax_loc,brmax,1,decomp_group)
#endif
    if(I_AM_NODE_ZERO) write(*,*)'-----poisson_for_pet_BEGIN----','br_max=',brmax,'dt',dt
     
      ! write(*,*)'before_new_nl,myid',myid, nlr(:,nyp-1,nzp/2,1) 

  !!! initial for nonlinear term
  !  do n=1,ndim
  !     do i = 0, nx
   !       do j = 0, ny
   !          do k = 0, nz

                      onlr = nlr
                      nlr = zero
   !          end do
   !       end do
   !    end do
   ! end do
  !! write(*,*)'onlr',onlr
    if(I_AM_NODE_ZERO)write(*,'(A50,I2)')'CONSERVATIVE: performing del dot uu '

      ! write(*,*)myid,'bbx1', br(2,1,5,3)
    if(.not.only_dem)then
        
  !!// compute nonliner, add into br
    
	    call form_nl_AB2
  
      ! call compute_fluxes  !!//gai
    end if
       write(*,*)'bbx11',myid, br(2,8,8,1),br(2,1,5,2)
    
     !-----------------------------------------------------------------------
    !	store nonlinear terms in forcing domain


    nlmean(1:ndim) = zero
    if(debug_check)then
       
          nlmean_loc(1) = SUM(nlr(i0s:i0e,1:nyp,1:nzp,1))
          nlmean_loc(2) = SUM(nlr(1:nxp,j0s:j0e,1:nzp,2))
          nlmean_loc(3) = SUM(nlr(1:nxp,1:nyp,k0s:k0e,3))
          do i =1,ndim
           GLOBAL_DOUBLE_SUM(nlmean_loc(i),nlmean(i),1,decomp_group)
          enddo
      if(dirichlet_xs.and.dirichlet_ys.and.dirichlet_zs) then   
       nlmean(1) = nlmean(1)/(mx-1)*(my-1)*(mz-1) !! gai 
       nlmean(2) = nlmean(2)/(mx-1)*(my-2)*(mz-1)
       nlmean(3) = nlmean(3)/(mx-1)*(my-1)*(mz-2)
      elseif(xperiodic.and.dirichlet_ys.and.dirichlet_zs) then
       nlmean(1) = nlmean(1)/(mx-1)*(my-1)*(mz-1) !! gai 
       nlmean(2) = nlmean(2)/(mx-1)*(my-2)*(mz-1)
       nlmean(3) = nlmean(3)/(mx-1)*(my-1)*(mz-2)
      endif   
      
       if(I_AM_NODE_ZERO) WRITE(*,'(A25,3(2x,g12.5))')'NLMEAN = ', nlmean
    end if

    
    !-----------------------------------------------------------------------
    !	calculate mean pressure gradient for PBC 
    !-----------------------------------------------------------------------
     if(xperiodic)   then
           call compute_mpg_at_n(rks) 
     elseif(.not.xperiodic) then
	    
	 if(mod(iglobstep,1).eq.0.and.nbody.gt.0) CALL compute_mpg_at_n(rks) !! ..comp drag
	 if(mod(iglobstep,10).eq.0.and.nbody.eq.0) CALL compute_mpg_at_n(rks)
     endif

     if(.not.only_dem)  CALL calc_visc  !!//gai need to change for pbc
	 
    !-----------------------------------------------------------------------
    !	calculate total pressure gradient terms 
    !-----------------------------------------------------------------------
    ! if(myid.eq.6) write(*,*)'bbx', br(:,1,:,1:3)
     if(.not.only_dem)  CALL calc_pgrad  
   ! write(*,*)myid,'bbx2',ppr(2,8,8,1),pr(2,8,8),pr(1,8,8),br(1,1,1,2),mpg(1)

    !-----------------------------------------------------------------------
    !	calculate body force terms 
    !-----------------------------------------------------------------------  
 
	 if(nbody.gt.0)then !!// notice there is particle or not
          call bcset(rks,user_x,user_y,user_z)      !!!// compute IBM force 
     end if
  
!write(*,*)'bbx3', br(1,1,1,2)
	
    if(I_AM_NODE_ZERO)write(*,'(A)')'END OF NONLINEAR and IBforce'

  end subroutine nonlinear

  
  
end Module nlcalc













