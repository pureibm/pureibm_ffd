#
#  This makefile is to compile PUReIBM-FFD code
#  The original makefile is from the PETsc
# --------------------------------------------------
#
#CFLAGS		 = -I/PETSc3/geodynamics/PetscSimulationsViewers/src
CFLAGS       =
FFLAGS		 =  -ffree-line-length-4000
CPPFLAGS         =
FPPFLAGS         =
LOCDIR		 = src/snes/examples/tutorials/
MANSEC           = 
  
DIRS		 = ex10d

# the two lines below must be here

#include ${PETSC_DIR}/conf/variables
#include ${PETSC_DIR}/conf/rules
include ${HOME}/local/petsc-3.4.3/conf/variables
include ${HOME}/local/petsc-3.4.3/conf/rules
# deal with multiple source files here
# the order of the files *.o should follow the same order of "OBJS"

  f90module.o :\
  f90module.F90
  
  dtibm_main_scal_flo.o :\
  dtibm_main_scal_flo.F90
    
  vrand.o:\
  vrand.F90
  
  ranstd.o:\
  ranstd.F90
  
  ransed.o:\
  ransed.F90 
  
  ranu2.o:\
  ranu2.F90\
  vrand.F90
  
  rann2.o:\
  rann2.F90\
  ranu2.F90\
  ranstd.F90
  
  ranjn.o:\
  ranjn.F90\
  rann2.F90
  
  precision.o :\
  precision.F90
  
  constants.o :\
  constants.F90\
  precision.F90
  
  global_data.o :\
  global_data.F90\
  constants.F90\
  precision.F90
 
  petsc_poisson.o :\
  petsc_poisson.F90\
  global_data.F90\
  f90module.F90\
  include_petsc.h\
  ibm.h 
     
  scalar_data.o :\
  scalar_data.F90\
  constants.F90\
  precision.F90\
  global_data.F90\
  ibm.h
  
  machine.o:\
  machine.F90\
  global_data.F90
  
  general_funcs.o:\
  general_funcs.F90\
  constants.F90\
  precision.F90
  
  string_funcs.o:\
  string_funcs.F90\
  precision.F90
  
  randomno.o:\
  randomno.F90\
  global_data.F90\
  constants.F90\
  precision.F90\
  ranu2.F90\
  ranjn.F90\
  ransed.F90\
  rann2.F90
   
  postproc_funcs.o:\
  postproc_funcs.F90\
  constants.F90\
  precision.F90\
  global_data.F90\
  general_funcs.F90\
  scalar_data.F90\
  randomno.F90
  
  hard_coll.o:\
  hard_coll.F90\
  constants.F90\
  precision.F90\
  global_data.F90\
  general_funcs.F90\
  postproc_funcs.F90\
  randomno.F90
  
  functions.o:\
  functions.F90\
  constants.F90\
  precision.F90\
  global_data.F90\
  general_funcs.F90\
  machine.F90\
  scalar_data.F90\
  ibm.h\
  include_petsc.h
  
  interpolation.o:\
  interpolation.F90\
  constants.F90\
  precision.F90
    
  errormesgs.o:\
  errormesgs.F90\
  precision.F90\
  global_data.F90\
  general_funcs.F90\
  string_funcs.F90
  
  initialize.o:\
  initialize.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  general_funcs.F90\
  scalar_data.F90\
  errormesgs.F90\
  include_petsc.h\
  ibm.h
  
  workarrays.o:\
  workarrays.F90\
  precision.F90\
  constants.F90\
  global_data.F90
  
  tridiagonal.o:\
  tridiagonal.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  ibm.h
  
  restart_funcs.o: \
  restart_funcs.F90\
  precision.F90\
  global_data.F90\
  general_funcs.F90\
  scalar_data.F90\
  errormesgs.F90\
  include_petsc.h\
  ibm.h
  
  dem_mod.o: \
  dem_mod.F90\
  precision.F90\
  constants.F90
  
  dependent_functions.o:\
  dependent_functions.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  general_funcs.F90\
  randomno.F90\
  dem_mod.F90\
  interpolation.F90\
  workarrays.F90\
  include_petsc.h\
  ibm.h
  
  soft_sphere_coll.o:\
  soft_sphere_coll.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  dem_mod.F90
  
  collision_mod.o:\
  collision_mod.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  general_funcs.F90\
  soft_sphere_coll.F90\
  randomno.F90\
  dem_mod.F90\
  interpolation.F90\
  ibm.h
  
  maternmod.o: \
  maternmod.F90\
  precision.F90\
  constants.F90\
  randomno.F90\
  collision_mod.F90\
  ibm.h
  
  geom_init.o:\
  geom_init.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  ibm.h
  
  scalar_init.o: \
  scalar_init.F90\
  global_data.F90\
  general_funcs.F90\
  scalar_data.F90\
  workarrays.F90\
  errormesgs.F90\
  ibm.h
  
  initconfig.o:\
  initconfig.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  scalar_data.F90\
  hard_coll.F90\
  collision_mod.F90\
  maternmod.F90\
  workarrays.F90\
  dependent_functions.F90\
  randomno.F90\
  postproc_funcs.F90\
  functions.F90\
  dem_mod.F90\
  include_petsc.h\
  ibm.h
  
  timestep.o:\
  timestep.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  scalar_data.F90\
  collision_mod.F90\
  workarrays.F90\
  include_petsc.h\
  ibm.h
  
  parallel_solver.o:\
  parallel_solver.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  workarrays.F90\
  tridiagonal.F90\
  general_funcs.F90\
  ibm.h\
  include_petsc.h
   
  initialize_flo.o: \
  initialize_flo.F90 \
  precision.F90\
  constants.F90\
  global_data.F90\
  general_funcs.F90\
  scalar_data.F90\
  workarrays.F90\
  dependent_functions.F90\
  collision_mod.F90\
  geom_init.F90\
  restart_funcs.F90\
  timestep.F90\
  initconfig.F90\
  parallel_solver.F90\
  ibm.h
  
  boundary_condition.o:\
  boundary_condition.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  workarrays.F90\
  collision_mod.F90\
  dependent_functions.F90\
  parallel_solver.F90\
  ibm.h\
  include_petsc.h
  
  nl_allflow.o:\
  nl_allflow.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  workarrays.F90\
  machine.F90\
  ibm.h
  
  nl.o: \
  nl.F90\
  nl_allflow.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  workarrays.F90\
  dependent_functions.F90\
  boundary_condition.F90\
  machine.F90\
  collision_mod.F90\
  parallel_solver.F90\
  ibm.h\
  include_petsc.h
  
  poisson.o: \
  poisson.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  tridiagonal.F90\
  workarrays.F90\
  f90module.F90\
  petsc_poisson.F90\
  boundary_condition.F90\
  parallel_solver.F90\
  ibm.h\
  include_petsc.h
  
  usteptime.o: \
  usteptime.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  dependent_functions.F90\
  boundary_condition.F90\
  petsc_poisson.F90\
  f90module.F90\
  parallel_solver.F90\
  include_petsc.h\
  ibm.h
  
  steptheflow.o: \
  steptheflow.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  dem_mod.F90\
  dependent_functions.F90\
  workarrays.F90\
  usteptime.F90 \
  poisson.F90\
  parallel_solver.F90\
  include_petsc.h\
  f90module.F90\
  ibm.h
  
  writeoutput.o: \
  writeoutput.F90\
  precision.F90\
  constants.F90\
  global_data.F90\
  errormesgs.F90\
  general_funcs.F90\
  dependent_functions.F90\
  postproc_funcs.F90\
  workarrays.F90\
  include_petsc.h\
  ibm.h
  
  bc_scalar.o: \
  bc_scalar.F90 \
  precision.F90\
  constants.F90\
  scalar_data.F90\
  general_funcs.F90\
  dependent_functions.F90\
  interpolation.F90\
  boundary_condition.F90\
  errormesgs.F90\
  workarrays.F90\
  parallel_solver.F90\
  ibm.h\
  f90module.F90\
  include_petsc.h
  
  nl_allphi.o: \
  nl_allphi.F90\
  precision.F90\
  constants.F90\
  scalar_data.F90\
  functions.F90\
  ibm.h
  
  nlphi.o: \
  nlphi.F90\
  precision.F90\
  constants.F90\
  scalar_data.F90\
  global_data.F90\
  nl_allphi.F90\
  bc_scalar.F90\
  f90module.F90\
  include_petsc.h\
  ibm.h
  
  stepthescalar.o:\
  stepthescalar.F90 \
  precision.F90\
  scalar_data.F90\
  tridiagonal.F90\
  nlphi.F90 \
  general_funcs.F90\
  petsc_poisson.F90\
  include_petsc.h\
  f90module.F90\
  ibm.h
  
  outputscalar.o: \
  outputscalar.F90 \
  precision.F90\
  scalar_data.F90\
  general_funcs.F90\
  restart_funcs.F90\
  postproc_funcs.F90\
  dependent_functions.F90\
  nlphi.F90\
  include_petsc.h\
  ibm.h
  
OBJS = \
\
f90module.o\
vrand.o\
ranstd.o\
ransed.o\
ranu2.o\
rann2.o\
ranjn.o\
precision.o\
constants.o\
global_data.o\
petsc_poisson.o\
scalar_data.o\
machine.o\
general_funcs.o\
string_funcs.o\
randomno.o\
postproc_funcs.o\
hard_coll.o\
functions.o\
interpolation.o\
errormesgs.o\
initialize.o\
workarrays.o\
tridiagonal.o\
restart_funcs.o\
dem_mod.o\
dependent_functions.o\
soft_sphere_coll.o\
collision_mod.o\
maternmod.o\
geom_init.o\
scalar_init.o\
initconfig.o\
timestep.o\
parallel_solver.o\
initialize_flo.o\
boundary_condition.o\
nl_allflow.o\
nl.o\
poisson.o\
usteptime.o\
steptheflow.o\
writeoutput.o\
bc_scalar.o\
nl_allphi.o\
nlphi.o\
stepthescalar.o\
outputscalar.o\
dtibm_main_scal_flo.o


main: ${OBJS} chkopts
	-${FLINKER} -o main ${OBJS} ${PETSC_LIB}
	${RM} ${OBJS} *.mod


#
#  The SGI parallelizing compiler generates incorrect code by treating 
#  the math functions (such as sqrt and exp) as local variables. The 
#  sed below patches this.

#  if you compiler just one file, then use this command below
# iex5f90: ex5f90.o  chkopts
#	-${FLINKER} -o ex5f90 ex5f90.o ${PETSC_SNES_LIB}
#	${RM} ex5f90.o

