
#define SQR(a) ((a)*(a))


!!#define PARALLEL_START() CALL MPI_INIT(err_code)
!!#define PARALLEL_FINISH()CALL MPI_FINALIZE(err_code)  
!!#define SOLVE(initialize,presvelflag) CALL PARALLEL_SOLVE(initialize,presvelflag)
!!#define GET_NPROCS(comm,psize) CALL MPI_COMM_SIZE(comm,psize,err_code)
!!#define GET_PROCESSOR_RANK(comm,rank) CALL MPI_COMM_RANK(comm, rank, err_code)
#define BROADCAST_INT(item,nitems,from,comm)   CALL MPI_BCAST(item,nitems,MPI_INTEGER,from,comm,err_code)
#define BROADCAST_DOUBLE(item,nitems,from,comm)   CALL MPI_BCAST(item,nitems,MPI_DOUBLE_PRECISION,from,comm,err_code)
#define BROADCAST_REAL(item,nitems,from,comm)   CALL MPI_BCAST(item,nitems,MPI_DOUBLE_REAL,from,comm,err_code)
#define BROADCAST_CHARARR(str,strlen,from,comm)   CALL MPI_BCAST(str,strlen,MPI_CHARACTER,from,comm,err_code)
#define BROADCAST_LOGICAL(item,nitems,from,comm)   CALL MPI_BCAST(item,nitems,MPI_LOGICAL,from,comm,err_code)
#define BROADCAST_STRING(str,strlen,from,comm)	\
  if(I_AM_NODE_ZERO) strlen = LEN_TRIM(str); BROADCAST_INT(strlen,1,from,comm); BROADCAST_CHARARR(str,strlen,from,comm)
#define SEND_STRING(str,strlen,to,sitag,sctag,comm) \
  strlen = LEN_TRIM(str); CALL MPI_SEND(strlen,1,MPI_INTEGER,to,sitag,comm,err_code); CALL MPI_SEND(str,strlen,MPI_CHARACTER,to,sctag,comm,err_code)
#define RECV_STRING(str,strlen,from,ritag,rctag,comm,status) \
  CALL MPI_RECV(strlen,1,MPI_INTEGER,from,ritag,comm,status,err_code); CALL MPI_RECV(str,strlen,MPI_CHARACTER,from,rctag,comm,status,err_code)

!! #define SEND_REAL_ARRAY(arr,arrlen,to,stag,comm) CALL MPI_SEND(arr,arrlen,MPI_DOUBLE_PRECISION,to,stag,comm,err_code)
!! #define RECV_REAL_ARRAY(arr,arrlen,from,rtag,comm,status)CALL MPI_RECV(arr,arrlen,MPI_DOUBLE_PRECISION,from,rtag,comm,status,err_code)


!!#define CREATE_CART_TOPOLOGY(commold,ndims,psize,isperiodic,reorder,newcomm) CALL MPI_CART_CREATE(commold,ndims,psize,isperiodic,reorder,newcomm,err_code)
!!#define GET_SHIFT_PROCS(comm,dir,shift,src,dest) CALL MPI_CART_SHIFT(comm,dir,shift,src,dest,err_code)
#define GLOBAL_INT_SUM(sendbuf,recvbuf,nitems,comm) CALL MPI_ALLREDUCE(sendbuf,recvbuf,nitems,MPI_INTEGER,MPI_SUM,comm,err_code)
#define GLOBAL_DOUBLE_SUM(sendbuf,recvbuf,nitems,comm) CALL MPI_ALLREDUCE(sendbuf,recvbuf,nitems,MPI_DOUBLE_PRECISION,MPI_SUM,comm,err_code)
#define GLOBAL_COMPLEX_SUM(sendbuf,recvbuf,nitems,comm) CALL MPI_ALLREDUCE(sendbuf,recvbuf,nitems,MPI_DOUBLE_COMPLEX,MPI_SUM,comm,err_code)
#define GLOBAL_DOUBLE_MAX(sendbuf,recvbuf,nitems,comm) CALL MPI_ALLREDUCE(sendbuf,recvbuf,nitems,MPI_DOUBLE_PRECISION,MPI_MAX,comm,err_code)
#define GLOBAL_INT_MIN(sendbuf,recvbuf,nitems,comm) CALL MPI_ALLREDUCE(sendbuf,recvbuf,nitems,MPI_INTEGER,MPI_MIN,comm,err_code)
#define I_AM_IN_THIS_PROC(ii) ((xstart-1.le.ii).and.(ii.le.xend+1))

#define LOCAL_INDEX_X(ii) ii=(ii-xstart+1)
#define LOCAL_INDEX_Y(ii) ii=(ii-ystart+1)
#define LOCAL_INDEX_Z(ii) ii=(ii-zstart+1)

#define GLOBAL_INDEX_X(ii)(ii+xstart-1)
#define GLOBAL_INDEX_Y(ii)(ii+ystart-1)
#define GLOBAL_INDEX_Z(kk)(kk+zstart-1)
#define GLOBAL_INDEX(ii)(ii+xstart-1)

#define XR_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mx+(ii)-1
#define XL_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mx))iimage = (ii)-(mx-1)
#define YR_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = my+(ii)-1
#define YL_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.my))iimage = (ii)-(my-1)
#define ZR_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mz+(ii)-1
#define ZL_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mz))iimage = (ii)-(mz-1)

#define XR_PERIODIC_POINT_IMAGE2(ii,iimage) if(((ii).le.1))iimage = mx+(ii)-1
#define XL_PERIODIC_POINT_IMAGE2(ii,iimage) if(((ii).ge.mx-1))iimage = (ii)-(mx-1)
#define YR_PERIODIC_POINT_IMAGE2(ii,iimage) if(((ii).le.1))iimage = my+(ii)-1
#define YL_PERIODIC_POINT_IMAGE2(ii,iimage) if(((ii).ge.my-1))iimage = (ii)-(my-1)
#define ZR_PERIODIC_POINT_IMAGE2(ii,iimage) if(((ii).le.1))iimage = mz+(ii)-1
#define ZL_PERIODIC_POINT_IMAGE2(ii,iimage) if(((ii).ge.mz-1))iimage = (ii)-(mz-1)


#define WEST_PERIODIC_IMAGE_MOD(ii,iimage,x,ximage) if(((ii).le.1))then ;iimage = mxf+(ii)-1;ximage = mxf+(x)-1;endif

#define XL_PERIODIC_IMAGE_PRES(ii,iimage,x,ximage) if(((x).lt.1))then ;iimage = mx+(ii)-1;ximage = mx+(x)-1;endif
#define XR_PERIODIC_IMAGE_PRES(ii,iimage,x,ximage) if(x.ge.mx)then ;iimage = (ii)-(mx-1);ximage = (x)-(mx-1);endif
#define YL_PERIODIC_IMAGE_PRES(ii,iimage,x,ximage) if(((x).lt.1))then ;iimage = my+(ii)-1;ximage = my+(x)-1;endif
#define YR_PERIODIC_IMAGE_PRES(ii,iimage,x,ximage) if(((x).ge.my))then ;iimage = (ii)-(my-1);ximage = (x)-(my-1);endif
#define ZL_PERIODIC_IMAGE_PRES(ii,iimage,x,ximage) if(((x).lt.1))then ;iimage = mz+(ii)-1;ximage = mz+(x)-1;endif
#define ZR_PERIODIC_IMAGE_PRES(ii,iimage,x,ximage) if(((x).ge.mz))then ;iimage = (ii)-(mz-1);ximage = (x)-(mz-1);endif

!! #define RSENDRECV(sitem,nsitems,to,stag,ritem,nritems,from,rtag,comm,status) CALL MPI_SENDRECV(sitem,nsitems,MPI_DOUBLE_PRECISION,to,stag,ritem,nritems,MPI_DOUBLE_PRECISION,from,rtag,comm,status,err_code)
!! #define CSENDRECV(sitem,nsitems,to,stag,ritem,nritems,from,rtag,comm,status) CALL MPI_SENDRECV(sitem,nsitems,MPI_DOUBLE_COMPLEX,to,stag,ritem,nritems,MPI_DOUBLE_COMPLEX,from,rtag,comm,status,err_code)

#define POINT_IN_X_VEL_GRID(x) ((xstart.le.x).and.(x.lt.xend+1))
#define POINT_IN_Y_VEL_GRID(x) ((ystart.le.x).and.(x.lt.yend+1))
#define POINT_IN_Z_VEL_GRID(x) ((zstart.le.x).and.(x.lt.zend+1))
#define CELL_IN_VEL_GRID(x) ((xstart.le.x).and.(x.le.xend))

#define POINT_IN_X_VEL_GRID2(x) ((xstart-1.le.x).and.(x.le.xend+1))
#define POINT_IN_Y_VEL_GRID2(x) ((ystart-1.le.x).and.(x.le.yend+1))
#define POINT_IN_Z_VEL_GRID2(x) ((zstart-1.le.x).and.(x.le.zend+1))
#define POINT_IN_X_VEL_GRID3(x) ((xstart.le.x).and.(x.le.xend))
#define POINT_IN_Y_VEL_GRID3(x) ((ystart.le.x).and.(x.le.yend))
#define POINT_IN_Z_VEL_GRID3(x) ((zstart.le.x).and.(x.le.zend))


#define POINT_IN_X_PRESS_GRID(x) ((xstart.le.x).and.(x.lt.(xend+1)))
#define POINT_IN_Y_PRESS_GRID(x) ((ystart.le.x).and.(x.lt.(yend+1)))
#define POINT_IN_Z_PRESS_GRID(x) ((zstart.le.x).and.(x.lt.(zend+1)))
#define CELL_IN_PRESS_GRID(x) (((xstart-1).le.x).and.(x.lt.(xend)))

#define POINT_IN_X_PROC(x) ((xstart.le.x).and.(x.le.xend))
#define POINT_IN_Y_PROC(x) ((ystart.le.x).and.(x.le.yend))
#define POINT_IN_Z_PROC(x) ((zstart.le.x).and.(x.le.zend))

#define CELL_IN_PROC_X(ii) ((xstart-1.le.ii).and.(ii.le.xend+1))
#define CELL_IN_PROC_Y(ii) ((ystart-1.le.ii).and.(ii.le.yend+1))
#define CELL_IN_PROC_Z(ii) ((zstart-1.le.ii).and.(ii.le.zend+1))

#define RPR_POINT_IN_PROC(x) ((xstart-2.le.x).and.(x.lt.xend+2))
#define RPR_CELL_IN_PROC(x) ((xstart-2.le.x).and.(x.lt.xend+2))

#define EAST_NO_MANS_LAND(x) (x).eq.(xend)
#define WEST_NO_MANS_LAND(x) (x).eq.(xstart-1)

!!#define CONCAVE(x,n,m) (x(n).gt.xc(m,n))

!!#define CREATE_2D_LSLICE(count,blocklength,stride,newtype) CALL MPI_TYPE_VECTOR(count,blocklength,stride,MPI_LOGICAL,newtype,err_code)

!! #define VECSENDRECV(sitem,nsitems,vectype,to,stag,ritem,nritems,from,rtag,comm,status) CALL MPI_SENDRECV(sitem,nsitems,vectype,to,stag,ritem,nritems,vectype,from,rtag,comm,status,err_code)
!! #define VECSENDRECV2(sitem,nsitems,vectype1,to,stag,ritem,nritems,vectype2,from,rtag,comm,status) CALL MPI_SENDRECV(sitem,nsitems,vectype1,to,stag,ritem,nritems,vectype2,from,rtag,comm,status,err_code)
#define BARRIER(comm) CALL MPI_BARRIER(comm,err_code)




#define I_AM_NODE_ZERO (myid.eq.0)
#define I_AM_LAST_PROC (xend.eq.mx1)
#define CELL_IN_BOX(ii) ((1.le.ii).and.(ii.le.mx1))
#define WEST_PERIODIC_IMAGE(ii,iimage,x,ximage) if(((ii).lt.1))then ;iimage = mxf+(ii)-1;ximage = mxf+(x)-1;endif
#define EAST_PERIODIC_IMAGE(ii,iimage,x,ximage) if(((ii).ge.mxf))then ;iimage = (ii)-(mxf-1);ximage = (x)-(mxf-1);endif
#define EAST_PERIODIC_IMAGE_MOD(ii,iimage,x,ximage) if(((ii).ge.mxf-1))then ;iimage = (ii)-(mxf-1);ximage = (x)-(mxf-1);endif
