
!    PUReIBM-FFD is a three-dimensional fully finite-differnce particle-resolved
!    direct numerical simulation solver for detailed analysis of flow and heat and mass transfer problem. PUReIBM-FFD
!    is a continuum Navier-Stokes and scalar solvers based on Cartesian grid that utilizes
!    Immeresed Boundary method to represent particle surfuces. The details about the solvers
!    can be found in the below papers in SUBRAMANIAM's group. 
!    Copyright (C) 2015, Shankar Subramaniam, Rahul Garg, Sudheer Tenneti, Bo Sun, Mohammad Mehrabadi
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!    For acknowledgement, please refer to the following publications:
!   
!     (1) TENNETI, S. and SUBRAMANIAM, S., 2014, Particle-resolved direct numerical
!         simulation for gas-solid flow model development. Annu. Rev. Fluid Mech.
!         46 (1) 199-230.
!     (2) B. Sun,2015, Modeling Heat and Mass Transfer in Reacting Gas-solid Flow 
!         Using Particle-resolved Direct Numerical Simulation. PhD thesis, Iowa State University.

!!ROUTINE TO CALCULATE THE IMMERSED BOUNDARY FORCE FOR THE SCALAR FIELD
!Author: RAHUL GARG
MODULE bc_scalar
#include "ibm.h"

  USE precision 
  USE constants 
  
  USE scalar_data
  USE  bcsetarrays !, ONLY : gradphi=>ppr,fr,diffn 
  USE general_funcs
  USE dependent_functions
  USE interpolation
  USE parallel_solver
  use f90module
  
  IMPLICIT NONE 
  PRIVATE 
  
  REAL(prcn) ::  drm, rad, rad2, lc_time, lc_dist, da(3)
  REAL(prcn) :: sumforcepoint, sumforcenode,fphirmeanfluid(1)&
       &,fphirmeanloc(1),fphirmeanfluidloc(1)
  
  integer ::  is(ndim),iii(ndim),io(ndim), io2(ndim)
  integer, save :: unitflux, unitfor , unitfluxsum, unitsurftemp, unitnuss
  PUBLIC :: bcsetscal  , interpolate_phidata_stag,calc_comp

CONTAINS 
  SUBROUTINE bcsetscal(rks,user)

    USE errormesgs
    USE general_funcs
    USE nlarrays !
    USE boundary_condition
    IMPLICIT NONE 
   
#include "include_petsc.h" 

    !-----------------------------------------------------------------------
    !     local variables
	!!!!!!!!!
    	
    type (userctx) user
    INTEGER, INTENT(IN) :: rks
    INTEGER ::  ioffset,isp
    INTEGER :: i,j,k,l,m,n,sp, iphs
    REAL(prcn) ::  volume, area, upimod, utemp
    REAL(prcn) ::  tempt, areabdy(nbody), tempor,&
         & phisurface_tmp(nbody,nspmx), area_spec1, flux_tmp,&
         & nusselt_avg(4), source_mean(nspmx),&
         & tmp_nu_error_array(nerr_steps), sourcenuss(1) 


    !Diagnostic Variables 
    !REAL(prcn) ::  tempor1, tempor2

    CHARaCTER*80 :: FILENAME1, FILENAME2, FILENAME3, FILENAME4 
    CHARACTER(LEN=80) :: formfile
    LOGICAL:: filexist, isopen
#if PARALLEL
    REAL(prcn) :: frsumloc,frsum
    COMPLEX(prcn) :: fsumloc,fsum
    LOGICAL :: add_partition_force
#endif
!!$    unitfor = getnewunit(minunitno,maxunitno)
!!$
!!$    OPEN(unit=unitfor,file='forcing.diag',form='formatted'))

    formfile='formatted'
#if 0
    IF(I_AM_NODE_ZERO.and.first_pass) THEN 
       FILENAME1 = TRIM(RUN_NAME)//'_scalflux'//'.dat'
       FILENAME2 = TRIM(RUN_NAME)//'_scalfluxsum'//'.dat'
       FILENAME3 = TRIM(RUN_NAME)//'_surftemp'//'.dat'
       FILENAME4 = TRIM(RUN_NAME)//'_nusselt_comps'//'.dat'
       CALL SCALAR_RUN_TIME_FILE_OPENER(unitflux,FILENAME1, formfile)
       CALL SCALAR_RUN_TIME_FILE_OPENER(unitfluxsum, FILENAME2, formfile)
       CALL SCALAR_RUN_TIME_FILE_OPENER(unitnuss, FILENAME4, formfile)
       IF(LUMPED_CAP) THEN 
          unitsurftemp = getnewunit(minunitno, maxunitno)
          CALL SCALAR_RUN_TIME_FILE_OPENER(unitsurftemp,FILENAME3,formfile)
       end IF

       !first_pass=.false.
    ENDIF
#endif
#if 0
        do isp =1,nspmx
  do i= 1, nxp
  do j =1,nyp
  do k = 1,nzp
  if(i.eq.4.and.j.eq.4.and.k.eq.4)write(*,*)'whatbc22',nlphir(i,j,k,isp),onlphir(i,j,k,isp)
  enddo
  enddo
  enddo
  enddo
#endif
  
    drm = 1.d0 !its the value of the smearing length
    radm = 1.d0 !its the offset of the internal reversal point from the boundary point
    !-----------------------------------------------------------------------
    !     zero the force array

    Area_spec1 = (pi*(two*radbdy(1)*dx)**2)
     !!phirmean(1:nspmx) = phirmean(1:nspmx) + fphirmean(1:nspmx)*dt
     !!phigrad(1:nspmx) = zero
 
     fr = zero
             
 
    fphirmean(:) = zero 
    fphirmeanfluid(:) = zero 
    fphirmeanloc(:) = zero
    fphirmeanfluidloc(:) = zero 
    

  !! CALL calc_comp(rks) !!??

  !!!!!!!!!!!!! compute diffusion

   sdiffn = zero
     DO sp =1, nspmx ! Initialize the loop for all the species (nspmx)
       cdiff_scal  = gamma(sp)/dx/dx

       DO i=0,nxp+1 
          DO k=0,nzp+1
             DO j=0,nyp+1
               
                                                    
                  sdiffn(i,j,k,sp) = phir(i-1,j,k,sp)-two*phir(i,j,k,sp)+phir(i+1,j,k,sp) + &
	                           & phir(i,j-1,k,sp)-two*phir(i,j,k,sp)+phir(i,j+1,k,sp) + &
                                   & phir(i,j,k-1,sp)-two*phir(i,j,k,sp)+phir(i,j,k+1,sp)
                 
            
            !!!  if(i.eq.15.and.j.eq.15.and.k.eq.15)write(*,*)'diff',sdiffn(i,j,k,sp),phir(i-1,j,k,sp)-two*phir(i,j,k,sp)+phir(i+1,j,k,sp),phir(i-1,j,k,sp),two*phir(i,j,k,sp),phir(i+1,j,k,sp)                    
                     
              
                  sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)*cdiff_scal

              !!!!!!!!!! graphi      
          
     
                        ppr(i,j,k,1) = (phir(i+1,j,k,sp)-phir(i-1,j,k,sp))/(two*dx)
   
                        ppr(i,j,k,2) = (phir(i,j+1,k,sp)-phir(i,j-1,k,sp))/(two*dx)

                        ppr(i,j,k,3) = (phir(i,j,k+1,sp)-phir(i,j,k-1,sp))/(two*dx)
         
     
               ENDDO
             ENDDO
                    
          END DO
       ENDDO !sp

  !!!!! !!!!!
#if 0   
  do isp =1,nspmx
  do i= 1, nxp
  do j =1,nyp
  do k = 1,nzp
   if(i.eq.4.and.j.eq.4.and.k.eq.4)write(*,*)'whatbcdif',nlphir(i,j,k,isp),onlphir(i,j,k,isp)
  enddo
  enddo
  enddo
  enddo
#endif
 
  !!  write(*,*) diffn(1:mx,my/2,mz/2,1)
    sumforcepoint = 0.d0
    sumforcenode = 0.d0

    !    sum_flux_nm1(1:nbody) = zero

    flux_global(:) = zero 
    flux_global2(:) = zero 


   ! ---------------------------------------------------------------------
    !     loop over all bodies

    DO m = 1, nbody !Loop over bodies
       iphs = 1!part_array(m)%iphs
       
       nbnd = phase_array(iphs)%nbnd
       nrpr = phase_array(iphs)%nrpr
       
       bndarray => phase_array(iphs)%bndpts
       
       da(1)=4.*pi*(radbdy(m)*dx)**2./float(nbnd)
       da(2)=4.*pi*(radibdy(m)*dx)**2./float(part_array(m)%nrpr_active)
       da(3)=4.*pi*(radobdy(m)*dx)**2./float(nbnd)
       areabdy(m) = 4.*pi*(radbdy(m)*dx)**2

       !Write(*,*)'body = ', m 
       !     Calculate the surface area represented by each boundary point and 
       !     also do the same for each internal reversal point.

       !     Check if the forcing is conserved at the Boundary points

       flux_body(m,:) = zero
       flux_body2(m,:) = zero


      CALL calc_bndrev_data(m,rks) !phir,nlphir,onlphir, m)!force the boundary pts

       if(dorpr)  CALL  calc_inner_rev_data_stag(m,rks)
       
	   flux_global(:) = flux_global(:) + flux_body(m,:)*da(1)
#if 0 
       flux_global2(:) = flux_global2(:) + flux_body2(m,:)*da(1)
#endif

    END DO !end the body loop
      ! write(*,*)"check",fr(3,4,4:7,1)
!!!!!!!!!!!! communication

       CALL communicate_forcing_scal(user)

!!!!!!!!!!!!
#if 0           
  do isp =1,nspmx
  do i= 1, nxp
  do j =1,nyp
  do k = 1,nzp
   if(i.eq.4.and.j.eq.4.and.k.eq.4)write(*,*)'what1',nlphir(i,j,k,isp),onlphir(i,j,k,isp)
  enddo
  enddo
  enddo
  enddo
#endif 

    do isp = 1, nspmx
       GLOBAL_DOUBLE_SUM(fphirmeanloc(isp),fphirmean(isp),1,decomp_group)
    end do
   
    fphirmean(1:nspmx) = fphirmean(1:nspmx)/real((mx*my*mz),prcn)
    fphirmeanfluid(1:nspmx) = fphirmeanfluid(1:nspmx)&
         &/real(count_fluid,prcn)

    if(I_AM_NODE_ZERO)WRITE(*,'(A25,2(2x,g17.8))')'FPHIRMEAN ', fphirmean!, fphirmeanfluid

#if 0  

     
    IF(setphimean) then 
       
       sourcesink(1:nspmx) = -fphirmean(1:nspmx)/(one-maxvolfrac)
       
       sourcenuss(:) = sourcesink(:) + fphirmeanfluid(:)
       
       if(I_AM_NODE_ZERO)WRITE(*,'(A30,2x,g17.8)')'NOT EVOLVING PHIRMEAN: SOURCE = ', sourcesink(1:nspmx)
       !WRITE(*,'(A30,2(2x,g17.8))')'fphirmean = ', fphirmean(1:nspmx), SUM(fr(1:mx1,:,:,1))/(mx1*my*mz)!sourcesink(1:nspmx)
       !phirmean does not evolve in this case
    ELSE
       
       sourcesink(:) = -cf*(phistream-phi_fluid_mean(:)) + gamma(:)*flux_global(:)/(voldom*(one&
            &-maxvolfrac))
       if(include_frmeanfluid) then 
          WRITE(*,'("INCLUDE_FRMEANFLUID IS TRUE")') 
          sourcesink(:) = sourcesink(:) - fphirmeanfluid(:)/(one - maxvolfrac)
       end if

       sourcenuss(:) = gamma(:)*flux_global(:)/(voldom*(one&
            &-maxvolfrac))

       phirmean(1:nspmx) = phirmean(1:nspmx) + (fphirmean(1:nspmx)&
            &+sourcesink(1:nspmx)*(one-maxvolfrac))*dt 

       if(I_AM_NODE_ZERO)then
          WRITE(*,'(A)')'-----------------EVOLVING PHIRMEAN----------------------'

          WRITE(*,'(A25,1x,g17.8)') 'UNSTEADY TERM:', cf*(phistream-phi_fluid_mean(1))*(one-maxvolfrac)*area_spec1&
               &/(6.d0*pi*maxvolfrac*gamma(1)*(phisurf-phistream))
          
          WRITE(*,'(A25,1x,g17.8)') 'DIFF TERM:', -(flux_global(1)*char_length*char_length)&
               &/(6.d0*voldom*maxvolfrac*(phisurf-phistream))
          
          WRITE(*,'(A25,1x,g17.8)') 'SOURCE TERM:', -(sourcesink(1)*(one-maxvolfrac)*area_spec1)&
               &/(6.d0*pi*maxvolfrac*gamma(1)*(phisurf-phistream))
          
          if(abs(frmeanfluid(1)).gt.zero) WRITE(*,'(A,2x,g17.8)') 'SOURCE/fphirmeanfluid', sourcesink(1)/(-fphirmeanfluid(:)/(one - maxvolfrac))
       end if
    end IF
    
    nusselt_avg(1) = -(sourcesink(1)*(one-maxvolfrac)*area_spec1)&
         &/(6.d0*pi*maxvolfrac*gamma(1)*(phisurf-phistream))
    nusselt_avg(2) = -(sourcenuss(1)*(one-maxvolfrac)*area_spec1)&
         &/(6.d0*pi*maxvolfrac*gamma(1)*(phisurf-phistream))
#endif

    if(I_AM_NODE_ZERO)then

       PRINT*,'flux_global = ', flux_global, flux_body(1,:)!, flux_global2

    end if
    
    nusselt_avg(3) = -(flux_global(1)*char_length*char_length)&
         &/(6.d0*voldom*maxvolfrac*(phisurf-phistream))

    !if(dorpr) then 
!!$    nusselt_avg(4) = -(flux_global2(1)*dchar*dchar)&
!!$         &/(6.d0*voldom*vol_frac1*(phisurf-phistream))
    !else
    !end if

    if(ABS(nusselt_avg(3)).gt.zero) then
       nu_error = ABS(nusselt_avg(3)-nu_old)/nusselt_avg(3)
    ELSE
       nu_error  = 1.d0
    end if


    if(rks.eq.itrmax) then 
       !Rearrange the ferror_array array so that the last entry is flushed out
       tmp_nu_error_array(1:nerr_steps) = nu_error_array(1:nerr_steps)
       nu_error_array(2:nerr_steps) = tmp_nu_error_array(1:nerr_steps-1)
       nu_error_array(1) = nu_error
       !PRINT*,'FERROR_A =', FERROR_ARRAY
       nu_error_hist = SUM(nu_error_array(1:nerr_steps))/nerr_steps
    end if
    
    if(I_AM_NODE_ZERO)WRITE(*,'(A25,6(2x,g14.7))')'NU and ERROR  = ', nusselt_avg(3), nu_error
    nu_old = nusselt_avg(3)

#if 0
    
    add_partition_force = .TRUE.
    !if(xstart.eq.1)add_partition_force = .FALSE.    
    DO isp = 1, nspmx 
       do j = 1, my 
          do k = 1, mz
          
          end do
       end do
       !frsumloc = SUM(fr(1:nx,:,:,1))
       !GLOBAL_DOUBLE_SUM(frsumloc,frsum,1,decomp_group)
    end DO
    
    !if(I_AM_NODE_ZERO) WRITE(*,'(A25,3(2x,g17.8))')'fphirmean from domain=', frsum/(mx1&
    !     &*my*mz)
#endif

 !!!-----------------------------------------

   DO sp =1, nspmx
    if(sp .gt.3) stop 'not right'
     DO i=1,nxp            !loop over planes   
          do j = 1, nyp 
             do k = 1, nzp
                
                br(i,j,k,sp)= br(i,j,k,sp) + fr(i,j,k,sp) 
         ! write(*,*)"fr",i+xstart-1,j+ystart-1,k+zstart-1,fr(i,j,k,sp),br(i,j,k,sp)
#if 0
               ! if(fluid_atijk(i,j,k)) then 

                !   fr(i,j,k,sp)  = fr(i,j,k,sp)+sourcesink(sp)*maxvolfrac
               ! else
              !    fr(i,j,k,sp) = fr(i,j,k,sp) - sourcesink(sp)*(one&
               !         &-maxvolfrac)
                   !	   source_mean(sp) = source_mean(sp)+fr(i,j,k,sp)!-sourcesink(sp)*(one-maxvolfrac)
                !end if
#endif                
             end do
          end do        
     ENDDO
       
   ENDDO ! sp


#if 0    
    fsumloc = SUM(ff(1:nx,1,1,1))
    GLOBAL_COMPLEX_SUM(fsumloc,fsum,1,decomp_group)
    if(I_AM_NODE_ZERO)WRITE(*,'(A25,10(2x,g17.8))')'AVG FLUC FORCE  = ',fsum/(mx1)
    WRITE(*,'(A25,10(2x,g17.8))')'AVG FLUC FORCING = '&
                   &,SUM(fr(1:mx-1,1,1,1))/(mx-1)
#endif
    
#if 0   
    if (I_AM_NODE_ZERO)then
       IF(rks.eq.itrmax) then 
          !Write(*,*)'NUSS NUM: ', nusselt_avg(3), unitfluxsum
          !READ(*,*)
          WRITE(unitflux,'(400(2x,e20.12))') (tscal)/t_conv, tscal/t_vis, tscal/t_diff,&
               & (-flux_body(i,1)/(phisurf-phistream), i=1,nbody) , &
               &-flux_global(1)/(phisurf-phistream)
          
          WRITE(unitnuss,'(20(2x,e20.12))')(tscal)/t_conv, tscal/t_vis, tscal&
               &/t_diff, nusselt_avg(3), heat_ratio(:)
          
!!$          WRITE(unitfluxsum,'(20(2x, e20.12))') t/t_conv, t/t_vis, t&
!!$               &/t_diff, nusselt_avg(3),heat_ratio(1), ABS(nusselt_avg(3)-nusselt_avg(1))/nusselt_avg(3), nu_error, phi_fluid_mean(1)/phistream
       end IF
       
    end if
#endif
    if (I_AM_NODE_ZERO) write(*,*)"bcset_finish"
  END SUBROUTINE bcsetscal
  
  SUBROUTINE calc_bndrev_data(m,rks)
    IMPLICIT NONE 
#include "include_petsc.h"
    INTEGER, INTENT(IN) :: rks, m

    REAL(prcn) ::  dfll(nspmx)

    REAL(prcn) ::  xl(ndim), phil(nspmx),philo(nspmx),philo2(nspmx),phili(nspmx)
    REAL(prcn) ::  nlphil(nspmx),onlphil(nspmx),xltemp
    INTEGER :: sp, i, j, k, n, l 
    REAL(prcn) :: rad, tempor(nspmx), gradphibnd(3,nspmx), normal(3),&
         & sourcell(nspmx),fluxloc(nspmx), perfac_scal(nspmx)

    INTEGER :: ib, ie, jb, je, kb, ke, onew, ii, jj, kk, phicelltemp

    Integer :: pcell(3)
    LOGICAL :: phiterm

    frombnd = .TRUE.
    fromrpr = .FALSE.
    fluxloc = zero

    DO  l=1,nbnd
       
       rad = zero
       phil(:)=zero
       nlphil(:)=zero
       onlphil(:)=zero
       dfll(:) = zero
       sourcell(:) = zero 

       DO n=1,ndim
          
          xl(n)=xc(m,n)+ bndarray(n,l)*radbdy(m)
          
          
          rad=rad+(bndarray(n,l)*radbdy(m))**2.0


       ENDDO

       rad = SQRT(rad)
       normal(1:3) = bndarray(1:3,l)
   
	xltemp = xl(1)
	    if(.not.POINT_IN_X_PRESS_GRID(xltemp))then
             
              if(xperiodic) then
	             XR_PERIODIC_POINT_IMAGE(xl(1),xltemp) 
                  !!  if(l.eq.402) write(*,*)"ttt", xltemp
                 ! if(xli(1).lt.1) xlo(1) = mx+xlo(1)-1.0d0
                     !! WEST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mx+(ii)-1
                 XL_PERIODIC_POINT_IMAGE(xl(1),xltemp) 
                 ! if(xli(1).ge.mx) xlo(1)= xlo(1)-(mx-1)
                     !! EAST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mxf))iimage = (ii)-(mx-1)
              endif
              if(.not.POINT_IN_X_PRESS_GRID(xltemp))   goto 2600
                   	     
            end if

        xl(1) = xltemp
           
        xltemp = xl(2)
             if(.not.POINT_IN_Y_PRESS_GRID(xltemp))then

                 if(yperiodic) then
                   ! if(xli(2).lt.1) xlo(2) = my+xlo(2)-1 
	              YR_PERIODIC_POINT_IMAGE(xl(2),xltemp)
                      
                          !! WEST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mxf+(ii)-1
                   ! if(xli(2).ge.my) xlo(2)= xlo(2)-(my-1)
                    YL_PERIODIC_POINT_IMAGE(xl(2),xltemp) 
                     
                          !! EAST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mxf))iimage = (ii)-(mxf-1)
                 endif
                 if(.not.POINT_IN_Y_PRESS_GRID(xltemp)) goto 2600
              end if

	  xl(2) =  xltemp
           
         xltemp = xl(3)  
       
	      if(.not.POINT_IN_Z_PRESS_GRID(xltemp))then
               if(l.eq.278) then 
                  ! write(*,*)myid,'ooo',xli
            
               endif
                    if(zperiodic) then
		       ZR_PERIODIC_POINT_IMAGE(xl(3),xltemp) 
                       ! if(xli(3).lt.1) xlo(3) = mz+xlo(3)-1
                        !! WEST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mxf+(ii)-1
                      ZL_PERIODIC_POINT_IMAGE(xl(3),xltemp) 
                       ! if(xli(3).ge.mz) xlo(3)= xlo(3)-(mz-1)
                       !! EAST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mxf))iimage = (ii)-(mxf-1)
                    endif
                  
                  if(.not.POINT_IN_Z_PRESS_GRID(xltemp)) 	  goto 2600
               end if 
         xl(3) = xltemp
      
         DO n=1,ndim
          

           if((xl(n)-FLOOR(xl(n))).lt.half) then
                is(n)=FLOOR(xl(n))-1
           
           else
                is(n)=FLOOR(xl(n))
           endif 
       

       ENDDO

       call interpolate_phidata_stag(is,xl, ib&
            &,ie,jb,je,kb,ke,phil,nlphil,onlphil,dfll, 0,onew,l) 
       
       if(DOBND)then
          DO sp = 1,nspmx 
             !surf_scal_value(m,l,sp) = phil(sp)
             tempor(sp) = zero
             if(phiterm)tempor(sp) = tempor(sp) +cf*(phil(sp)-phisurfall(m,sp))
             tempor(sp) = tempor(sp) +three/two*nlphil(sp)- two*onlphil(sp) 
             tempor(sp) = tempor(sp) - dfll(sp)
             tempor(sp) = tempor(sp)*da(1)*drm*dx
             sumforcepoint = sumforcepoint + tempor(sp)/dx**3.d0
          ENDDO
       END if

       do k = 1, onew
      
            
          do j = 1, onew
             
          
             do i = 1, onew
        
                DO sp = 1, nspmx
                    ii = ib+i-1
                    jj = jb+j-1
                    kk = kb+k-1

                   LOCAL_INDEX_X(ii)
                   LOCAL_INDEX_Y(jj)
		   LOCAL_INDEX_Z(kk)

                   IF(DOBND) then 
                      fr(ii,jj,kk,sp)=fr(ii,jj,kk,sp)+(weightp(i,j,k)&
                           &*tempor(sp))/dx**3.  
                      fphirmeanloc(sp) = fphirmeanloc(sp) + weightp(i,j,k)*tempor(sp)/dx**3. 
                      if(fluid_atijk_scalar(ii,jj,kk)) fphirmeanfluidloc(sp) =&
                           & fphirmeanfluidloc(sp) + (weightp(i,j,k)&
                           &*tempor(sp))/dx**3.   
                   END IF
            

                  gradphisten(i,j,k,:,sp) = ppr(ii,jj,kk,:)

                ENDDO
             ENDDO
          ENDDO
       ENDDO
    
    do sp = 1,nspmx
       
       do n = 1, ndim 
          gradphibnd(n,sp) = &
               & array_dot_product(gradphisten(1:onew,1:onew&
               &,1:onew,n,sp),weightp(1:onew,1:onew,1:onew))
       end do
       fluxloc(sp) = fluxloc(sp)+&
            & array_dot_product(gradphibnd(1:ndim,sp)&
            &,normal(1:ndim)) 
       
!!$          flux_body(m,sp) = flux_body(m,sp)+&
!!$               & array_dot_product(gradphibnd(1:ndim,sp)&
!!$               &,normal(1:ndim)) 
    end do

       !RINT*,'flux = ', flux_body(m,1)
          
2600 continue
      
  ENDDO    !close loop over all boundary points

    do sp=1,nspmx
       GLOBAL_DOUBLE_SUM(fluxloc(sp),flux_body(m,sp),1,decomp_group)
    end do

   frombnd = .FALSE.

 END SUBROUTINE calc_bndrev_data



  subroutine interpolate_phidata_stag(pc,pos,ib,ie,jb,je,kb,ke,ul,nll,onll,dfll,flag, onew, l)
   ! use scalar_data
   ! USE general_funcs
  !  USE interpolation

    implicit none 
    Integer, intent(in) :: pc(3), l
    Integer, intent(in) :: flag
    Real(prcn), Dimension(:), Intent(in) :: pos
    Integer, intent(out) :: ib, ie, jb,je,kb,ke, onew
    Real(prcn), Intent(out), Dimension(:) :: ul,nll,onll,dfll
    Integer :: i, j,k, ii,jj,kk, n, isp
    REAL(prcn) :: perfac_scal(nspmx), perfac_onl(nspmx)

    CALL set_interpolation_stencil(pc,ib,ie,jb,je,kb,ke&
         &,interp_scheme, onew) 
    if((ib.lt.1.or.ie.gt.mxf).AND.(.not.xperiodic)) Print*,'Error in i ....',ib,ie,pc,pos

    do k = 1, onew
       do j = 1, onew
          do i = 1, onew
             ii = ib+i-1
             jj = jb+j-1
             kk = kb+k-1
             gstencil(i,j,k,1) = ib+(i-1) + half
             gstencil(i,j,k,2) = jb+(j-1) + half
             gstencil(i,j,k,3) = kb+(k-1) + half
               
   
             LOCAL_INDEX_X(ii)
			 LOCAL_INDEX_Y(jj)
			 LOCAL_INDEX_Z(kk)

             do isp = 1, nspmx
                vsten(i,j,k,isp) = phir(ii,jj,kk,isp)
             end do
             !  if(flag.eq.0)write(*,*)"ulo",i,j,k,ii,jj,kk,phir(ii,jj,kk,1),pos(1:3)
            
             if(flag.eq.1) then 
                nlsten(i,j,k,1:nspmx)  = nlphir(ii,jj,kk,1:nspmx)
                onlsten(i,j,k,1:nspmx)= onlphir(ii,jj,kk,1:nspmx)
                dfsten(i,j,k,1:nspmx) =   sdiffn(ii,jj,kk,1:nspmx)
            !! if(ii.eq.15.and.jj.eq.15.and.kk.eq.15) 
		!!	write(*,*) myid,"rank",ii,jj,kk, nlphir(ii,jj,kk,1:nspmx),phir(ii,jj,kk,1)
             end if
            
          end do
       end do
    end do
          
    CALL interpolator(gstencil(1:onew,1:onew,1:onew,1:3),&
         & vsten(1:onew,1:onew,1:onew,1:nspmx),pos(1:ndim),ul(1:nspmx),onew,&
         & interp_scheme,weightp) 
    if(flag.eq.1) then 
       do n = 1, nspmx 
          nll(n) =  array_dot_product(nlsten(1:onew,1:onew,1:onew,n&
               &),weightp(1:onew,1:onew,1:onew)) 
          onll(n)=  array_dot_product(onlsten(1:onew,1:onew,1:onew,n&
               &),weightp(1:onew,1:onew,1:onew))
          dfll(n)=  array_dot_product(dfsten(1:onew,1:onew,1:onew,n&
               &),weightp(1:onew,1:onew,1:onew))
        !! write(*,*)dfll(n),nll(n),ul(n)
       end do

    end if
  !  Print*, 'nll in interpdata =', nll(1), nlsten
  end subroutine interpolate_phidata_stag

 SUBROUTINE calc_inner_rev_data_stag(m,rks)

   IMPLICIT NONE 
#include "include_petsc.h"

   REAL(prcn) ::  dfll(nspmx),  xlo(ndim),xli(ndim), xlo2(ndim)
   INTEGER, INTENT(IN) :: rks

   REAL(prcn) ::  xl(ndim), phil(nspmx),force_dist(3), tmppa, force_fl(3)
   REAL(prcn) ::  philo(nspmx),philo2(nspmx),phili(nspmx)
   REAL(prcn) ::  nlphil(nspmx),onlphil(nspmx), xltemp
   INTEGER, INTENT(in) :: m
   Integer :: pcell(3), itertemp,isp
   INTEGER :: sp, i, j, k,l, count_fl, count_so, n
   REAL(prcn) :: rad, tempor(nspmx), sourcell(nspmx)
   INTEGER :: ib, ie, jb, je, kb, ke, onew, ii, jj, kk,rprcountloc,&
        & rprcount,rprcom,rprcomloc, phicelltemp, rpreval, rprevalcount
   LOGICAL :: phiterm, add_force
#if 1
   INTEGER :: FOCUS_POINT, FOCUS_PARTICLE
  ! FOCUS_POINT = -1
  ! FOCUS_PARTICLE = -1
#endif

   frombnd = .FALSE.
   fromrpr = .TRUE.


   rprcount = 0
   rprcountloc = 0
   rprcom = 0
   rprcomloc = 0
 !!write(*,*)'nrpr=',nrpr,radibdy(m),radobdy(m),radbdy(m) 

 
   DO l=1,nrpr
     ! if(.NOT.PART_ARRAY(m)%if_rev(L)) GOTO 666
      rad = zero
      rad2 = zero

      DO sp=1,nspmx
         phili(sp)=zero
         philo(sp)=zero
         nlphil(sp)=zero
         onlphil(sp)=zero
         dfll(sp) = zero
         philo2(sp)=zero
         sourcell(sp) = zero 
      ENDDO

      DO  n=1,ndim

         !     location of internal points

         xli(n)=xc(m,n)+ bndarray(n,l)*radibdy(m)

         !location of external poi


         rad=rad+(bndarray(n,l)*radobdy(m))**2.

     
          !     location of external points

          xlo(n)=xc(m,n)+ bndarray(n,l)*radobdy(m)


          
      ENDDO !! n
       
     !!  xpi(1) = (xli(1)-0.5)
     !!  xpi(2:3) = xli(2:3)

          

         rad=dsqrt(rad)

   
   !!! justice
      
      	xltemp = xli(1)
	    if(.not.POINT_IN_X_PRESS_GRID(xltemp))then
             
              if(xperiodic) then
	             XR_PERIODIC_POINT_IMAGE(xli(1),xltemp) 
                  !!  if(l.eq.402) write(*,*)"ttt", xltemp
                  if(xli(1).lt.1) xlo(1) = mx+xlo(1)-1.0d0
                     !! WEST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mx+(ii)-1
                 XL_PERIODIC_POINT_IMAGE(xli(1),xltemp) 
                  if(xli(1).ge.mx) xlo(1)= xlo(1)-(mx-1)
                     !! EAST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mxf))iimage = (ii)-(mx-1)
              endif
              if(.not.POINT_IN_X_PRESS_GRID(xltemp))   goto 1666
                   	     
            end if

        xli(1) = xltemp
           
        xltemp = xli(2)
             if(.not.POINT_IN_Y_PRESS_GRID(xltemp))then

                 if(yperiodic) then
                    if(xli(2).lt.1) xlo(2) = my+xlo(2)-1 
	              YR_PERIODIC_POINT_IMAGE(xli(2),xltemp)
                      
                          !! WEST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mxf+(ii)-1
                    if(xli(2).ge.my) xlo(2)= xlo(2)-(my-1)
                    YL_PERIODIC_POINT_IMAGE(xli(2),xltemp) 
                     
                          !! EAST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mxf))iimage = (ii)-(mxf-1)
                 endif
                 if(.not.POINT_IN_Y_PRESS_GRID(xltemp)) goto 1666
              end if

	  xli(2) =  xltemp
           
         xltemp = xli(3)  
       
	      if(.not.POINT_IN_Z_PRESS_GRID(xltemp))then
               if(l.eq.278) then 
                  ! write(*,*)myid,'ooo',xli
            
               endif
                    if(zperiodic) then
		       ZR_PERIODIC_POINT_IMAGE(xli(3),xltemp) 
                        if(xli(3).lt.1) xlo(3) = mz+xlo(3)-1
                        !! WEST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).lt.1))iimage = mxf+(ii)-1
                      ZL_PERIODIC_POINT_IMAGE(xli(3),xltemp) 
                        if(xli(3).ge.mz) xlo(3)= xlo(3)-(mz-1)
                       !! EAST_PERIODIC_POINT_IMAGE(ii,iimage) if(((ii).ge.mxf))iimage = (ii)-(mxf-1)
                    endif
                  
                  if(.not.POINT_IN_Z_PRESS_GRID(xltemp))  goto 1666
               end if 
         xli(3) = xltemp


      DO  n=1,ndim

         !     location of internal points

           if((xli(n)-FLOOR(xli(n))).lt.half) then
                iii(n)=FLOOR(xli(n))-1
           
           else
                iii(n)=FLOOR(xli(n))
           endif 
     
          !     location of external points
          

          if((xlo(n)-FLOOR(xlo(n))).lt.half) then
          
            io(n)=FLOOR(xlo(n))-1
          else
            io(n)=FLOOR(xlo(n))
          endif
          
      ENDDO !! n  

            call interpolate_phidata_stag(io,xlo, ib,ie,jb,je,kb,ke,philo,nlphil,onlphil,dfll, 0,onew,l) 
           ! write(*,*)"ooo",l,philo(1)

            call interpolate_phidata_stag(iii,xli,ib,ie,jb,je,kb,ke,phili,nlphil,onlphil,dfll, 1,onew,l) 

           ! if(l==nrpr) write(*,*)'lll',l,dfll(1),nlphil(1),onlphil(1),phili(1),weightp
                     


#if 0 
         DO sp = 1, nspmx
            !Nu3(m,l,sp)=-(phisurfall(m,sp) - philo(sp))/  &
            !    & ((-radbdy(m)+radobdy(m))*dx)

            flux_body2(m,sp)= flux_body2(m,sp) + (-three*phisurfall(m,sp)&
                 &+four*philo(sp)-philo2(sp))/(two*(-radbdy(m)&
                 &+radobdy(m))*dx) 
            !    Print*,'Nu=', Nu3(m,l,sp), maxval(-Nu3(m,:,sp))/minval(-Nu3(m,:,sp))
         END DO
#endif

      
            DO sp=1,nspmx

               philo(sp) = phisurfall(m,sp)*(radobdy(m)-radibdy(m))-philo(sp)*(radbdy(m)-radibdy(m))
               philo(sp) = philo(sp)/(radobdy(m)-radbdy(m))
            !!   philo(sp) = -philo(sp)*(da(3)/da(2))
               phil(sp) = cf*(-philo(sp)+phili(sp))

            ENDDO
    

         !----------------------------------------------------------
         !     Interpolate the forcing using the interpolating
         ! factor on the boundary.

         !NOTE: IN ORDER TO PUT CORRECT FORCING ON THE GRID THE LAST
         ! INTERPOLATION SHUD HAVE BEEN PERFORMED ON THE INNER POINT OR
 
         DO sp = 1, nspmx
            tempor(sp) = zero
            tempor(sp) = tempor(sp) + phil(sp)
            tempor(sp) = tempor(sp) + three/two*nlphil(sp) - half*onlphil(sp)
          
            tempor(sp) = tempor(sp) - dfll(sp)
            
            tempor(sp) = tempor(sp)*da(2)*drm*dx  

            !fphirmean(sp) = fphirmean(sp) + tempor(sp)/dx**3.d0

            sumforcepoint = sumforcepoint + tempor(sp)/dx**3.d0
         ENDDO
       
    !!  write(*,*)myid,"pp",L,Tempor(1), dfll(1),phil(1),nlphil(1),philo(1),phili(1)
 
         count_fl = 0 
         force_fl = zero
 
        
         do k = 1, onew
  
        
            do j = 1, onew
                      

               do i = 1, onew

                  ii = ib+i-1
                  jj = jb+j-1
                  kk = kb+k-1
                    LOCAL_INDEX_X(ii)
		    LOCAL_INDEX_Y(jj)
		    LOCAL_INDEX_Z(kk)
              
                  if(fluid_atijk_scalar(ii,jj,kk)) then 
                     count_fl = count_fl+1
                     DO sp = 1, nspmx
                        tmppa = weightp(i,j,k)*tempor(sp)/dx**3.  
                        force_fl(sp) = force_fl(sp)+tmppa
                        
                     end DO
                    if(l.eq.280) then
                           write(*,*)myid,"280",ii,jj,kk,ii+xstart-1,jj+ystart-1,kk+zstart-1,ib,jb,kb,tmppa,force_fl(1)
                       endif
                  end if
                 !  if(ii.eq.15.and.jj.eq.15)write(*,*)'xilll',ii,jj,kk, &
                 !     & abs(abs(xli(1)-ii-0.5)-1)*abs(abs(xli(2)-jj-0.5)-1)*abs(abs(xli(3)-kk-0.5)-1),weightp(i,j,k), &
                 !     & phir(ii,jj,kk,1),fluid_atijk(ii,jj,kk),fluid_atijk_scalar(ii,jj,kk),&
                 !     & weightp(i,j,k)*tempor(1)/dx**3,sdiffn(ii,jj,kk,1)
                 ! if(fluid_atijk(ii,jj,kk).ne.fluid_atijk_scalar(ii,jj,kk)) write(*,*) 'fff',ii,jj,kk,xli(1:3)
             end do
            end do
         end do
     
         count_so = onew*onew*onew - count_fl
         force_dist(1:nspmx) = force_fl(1:nspmx)/real(count_so,prcn)
         
         add_force = .TRUE.
       
         do k = 1, onew
         

            do j = 1, onew
             
               
               do i = 1, onew
         
                  ii = ib+i-1
                  jj = jb+j-1      
                   kk = kb+k-1
                    LOCAL_INDEX_X(ii)
		    LOCAL_INDEX_Y(jj)
	    	    LOCAL_INDEX_Z(kk)

                  if(include_frmeanfluid)  then 
                     if(fluid_atijk_scalar(ii,jj,kk)) then
                        DO sp = 1, nspmx
                           fphirmeanfluidloc(sp) =&
                                & fphirmeanfluidloc(sp)  +weightp(i,j,k)&
                                &*tempor(sp)/dx**3.
                        End DO
                     endif
                     DO sp = 1, nspmx
                        fr(ii,jj,kk,sp)=fr(ii,jj,kk,sp)+weightp(i,j,k)&
                             &*tempor(sp)/dx**3.
                     End DO
                  else
                     if((.not.fluid_atijk_scalar(ii,jj,kk)).and.add_force) then
                        DO sp = 1, nspmx
                           fr(ii,jj,kk,sp)=fr(ii,jj,kk,sp)+weightp(i,j,k)&
                                &*tempor(sp)/dx**3  + force_dist(sp)
                        End DO
                   !    if(l.eq.280) then
                   !        write(*,*)"280",weightp(i,j,k)*tempor(1)/dx**3,force_dist(1)
                   !    endif
                   !      if(ii+xstart-1.eq.3.and.jj+ystart-1.eq.4.and.kk+zstart-1.eq.5)  write(*,*)myid,"pp",l,tempor(1),weightp(i,j,k),fr(ii,jj,kk,1)
                     end if
                  end if
                  
                  fphirmeanloc(1:nspmx) = fphirmeanloc(1:nspmx) + weightp(i,j,k)*tempor(1:nspmx)/dx**3. 
                  
                  !if(fluid_atijk(ii,jj,kk)) fphirmeanfluidloc(sp) =&
                  !    & fphirmeanfluidloc(sp) + (weightp(i,j,k)&
                  !   &*tempor(sp))/dx**3.   
               ENDDO
            ENDDO
         ENDDO
       
     
1666   continue

   ENDDO ! loop over reversal points

   ! fromrpr = .FALSE.
   ! write(*,*)diffn(:,my/2,mz/2,1)
#if 0   
    do i=0,nxp+1
    do j=0,nyp+1
    do k=0,nzp+1
     ! fr(i,j,k,1) = phir(i,j,k,1) + dt*
      write(*,*)myid,"check_fr",i+xstart-1,j+ystart-1,k+zstart-1, fr(i,j,k,1) 
    enddo
    enddo  
    enddo
#endif 

   

  END SUBROUTINE calc_inner_rev_data_stag

    SUBROUTINE calc_comp(rks)

    IMPLICIT NONE 
     INTEGER, INTENT(IN) :: rks
    INTEGER :: sp, i, j, k, ioffset,lend,lstart,isp
    Real(prcn) :: sumdiff1	  

    lstart = 1

#if PARALLEL
    lend = nx
#else
    lend = nx+1
#endif 
       sdiffn = zero   !!! it should be changed if nspmx > 3
       gradphi = zero
            do isp =1,nspmx
  do i= 1, nx
  do j =1,my-1
  do k = 1,mz-1
  if(i.eq.11.and.j.eq.10.and.k.eq.20)write(*,*)'whatdif22',nlphir(i,j,k,isp),onlphir(i,j,k,isp)
  enddo
  enddo
  enddo
  enddo
 
    DO sp =1, nspmx ! Initialize the loop for all the species (nspmx)
       cdiff_scal  = gamma(sp)/dx/dx

       DO i=1,mx-1 
          DO k=1,mz-1
             DO j=1,my-1
#if 0
                if(xperiodic) then
                  
                   if(i.eq.1) THEN
                      sdiffn(i,j,k,sp)=phir(nx,j,k,sp)*heat_ratio(sp)-2.*phir(i,j,k,sp)+ phir(i+1,j,k,sp)
                   else if(i.eq.mx-1) THEN
                      sdiffn(i,j,k,sp)=phir(i-1,j,k,sp)-two*phir(i,j,k,sp)+phir(1,j,k,sp)/heat_ratio(sp)
                   else
                      
                      sdiffn(i,j,k,sp)=phir(i-1,j,k,sp)-two*phir(i,j,k,sp)+phir(i+1,j,k,sp)
                   endif

	        else
                !!   diffn(i,j,k,sp)=diffn(i,j,k,sp)+(-phir(i-2,j,k,sp)+16.d0*phir(i-1,j,k,sp)-30.d0*phir(i,j,k,sp)+16*phir(i+1,j,k,sp)-phir(i+2,j,k,sp))/12.d0
                     sdiffn(i,j,k,sp)=phir(i-1,j,k,sp)-two*phir(i,j,k,sp)+phir(i+1,j,k,sp)
                endif
                if(yperiodic) then
                  
                   if(j.eq.1) THEN
                      sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,my-1,k,sp)-two*phir(i,j,k,sp)+ phir(i,j+1,k,sp)
                   else if(j.eq.my-1) THEN
                        
		      sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,j-1,k,sp)-two*phir(i,j,k,sp)+phir(i,1,k,sp)
                   else
                      
                      sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,j-1,k,sp)-two*phir(i,j,k,sp)+phir(i,j+1,k,sp)
                   endif
                  !!  diffn(i,j,k,sp)=diffn(i,j,k,sp)+(-phir(i,j-2,k,sp)+16.d0*phir(i,j-1,k,sp)-30.d0*phir(i,j,k,sp)+16*phir(i,j+1,k,sp)-phir(i,j+2,k,sp))/12.d0

	        else
             
                      sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,j-1,k,sp)-two*phir(i,j,k,sp)+phir(i,j+1,k,sp)
                endif

		if(zperiodic) then
                  
                   if(k.eq.1) THEN
                      sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,j,mz-1,sp)-two*phir(i,j,k,sp)+ phir(i,j,k+1,sp)
                   else if(k.eq.mz-1) THEN
                        
		      sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,j,k-1,sp)-two*phir(i,j,k,sp)+phir(i,j,1,sp)
                   else
                   !!  diffn(i,j,k,sp)=diffn(i,j,k,sp)+(-phir(i,j,k-2,sp)+16.d0*phir(i,j,k-1,sp)-30.d0*phir(i,j,k,sp)+16*phir(i,j,k+1,sp)-phir(i,j,k+2,sp))/12.d0  
                     sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,j,k-1,sp)-two*phir(i,j,k,sp)+phir(i,j,k+1,sp)
                   endif

	        else
                      sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)+phir(i,j,k-1,sp)-two*phir(i,j,k,sp)+phir(i,j,k+1,sp)
                endif
                
	              sdiffn(i,j,k,sp)=sdiffn(i,j,k,sp)*cdiff_scal
#endif
                !end of diffusion terms calculation 
                !beginnig of grad phi tems calculation
       
 

 
  if(i.eq.11.and.j.eq.10.and.k.eq.20)write(*,*)'whatdiff33',nlphir(i,j,k,sp),onlphir(i,j,k,sp)
  


	        if(xperiodic) then
                   if(i.eq.1) then
                      gradphi(i,j,k,1) = (phir(2,j,k,sp)-phir(mx-1,j,k,sp)*heat_ratio(sp))/(two*dx) 
					 


                   else if(i.eq.mx-1)then
                 
                        gradphi(i,j,k,1) = (phir(1,j,k,sp)/heat_ratio(sp)-phir(i-1,j,k,sp))/(two*dx)
                   ELSE
                        gradphi(i,j,k,1) = (phir(i+1,j,k,sp)-phir(i-1,j,k,sp))/(two*dx)
                   end if
                 else
	                 gradphi(i,j,k,1) = (phir(i+1,j,k,sp)-phir(i-1,j,k,sp))/(two*dx)
                endif 
	         if(yperiodic) then
                   if(j.eq.1) then
                   
			 gradphi(i,j,k,2) = (phir(i,2,k,sp)-phir(i,my-1,k,sp)*heat_ratio(sp))/(two*dx) 
             
                   else if(j.eq.my-1)then
                 
                        gradphi(i,j,k,2) = (phir(i,1,k,sp)/heat_ratio(sp)-phir(i,j-1,k,sp))/(two*dx)
                   ELSE
                        gradphi(i,j,k,2) = (phir(i,j+1,k,sp)-phir(i,j-1,k,sp))/(two*dx)
                   end if
                 else
		        gradphi(i,j,k,2) = (phir(i,j+1,k,sp)-phir(i,j-1,k,sp))/(two*dx)
                 endif
		 if(zperiodic) then
                   if(k.eq.1) then
                      
                        gradphi(i,j,k,3) = (phir(i,j,2,sp)-phir(i,j,mz-1,sp)*heat_ratio(sp))/(two*dx) 

                   else if(k.eq.mz-1)then
                 
                        gradphi(i,j,k,3) = (phir(i,j,1,sp)/heat_ratio(sp)-phir(i,j,k-1,sp))/(two*dx)
                   ELSE
                        gradphi(i,j,k,3) = (phir(i,j,k+1,sp)-phir(i,j,k-1,sp))/(two*dx)
                   end if
                 else
		        gradphi(i,j,k,3) = (phir(i,j,k+1,sp)-phir(i,j,k-1,sp))/(two*dx)
                 endif
                ENDDO
             ENDDO
                    
          END DO


       ENDDO !sp

   ! write(*,*) diffn(1:mx,my/2,mz/2,1) ! t, sumdiff1/(mxf*my*mz), sumdiff2/(mxf&
    !     &*my*mz), sumdiff3/(mxf*my*mz)
     END SUBROUTINE calc_comp

END MODULE bc_scalar
  
