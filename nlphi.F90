!    PUReIBM-FFD is a three-dimensional fully finite-differnce particle-resolved
!    direct numerical simulation solver for detailed analysis of flow and heat and mass transfer problem. PUReIBM-FFD
!    is a continuum Navier-Stokes and scalar solvers based on Cartesian grid that utilizes
!    Immeresed Boundary method to represent particle surfuces. The details about the solvers
!    can be found in the below papers in SUBRAMANIAM's group. 
!    Copyright (C) 2015, Shankar Subramaniam, Rahul Garg, Sudheer Tenneti, Bo Sun, Mohammad Mehrabadi
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!    For acknowledgement, please refer to the following publications:
!   
!     (1) TENNETI, S. and SUBRAMANIAM, S., 2014, Particle-resolved direct numerical
!         simulation for gas-solid flow model development. Annu. Rev. Fluid Mech.
!         46 (1) 199-230.
!     (2) B. Sun,2015, Modeling Heat and Mass Transfer in Reacting Gas-solid Flow 
!         Using Particle-resolved Direct Numerical Simulation. PhD thesis, Iowa State University.
MODULE nlphi
#include "ibm.h"
  Use precision
  USE scalar_data
  
  USE global_data ! ONLY : mx, my, mz, my2, mxf, foffset, aliasflag, nx
  USE nl_allphi
  USE bc_scalar  
  use f90module

  IMPLICIT NONE
!!#include "include_petsc.h" (warning : never put include_petsc.h here) 

  PUBLIC ::  compute_phim_heat_ratio,nlphi_graduphi 
  !--------------------------------------------------------------------

  !calculate the nonlinear (convective) terms in the Scalar equation
 
  !-----------------------------------------------------------------------
CONTAINS 
  SUBROUTINE nlphi_graduphi(rks,user)

    IMPLICIT NONE 
#include "include_petsc.h"   
     type (userctx)   user
 
    INTEGER, INTENT(in) ::  rks

    REAL(prcn) :: nlphimean(nspmx),nlphimeanloc(nspmx) !,urr(1:mx,1:my,1:mz,1:3),e_u(1:3)

    INTEGER i,j,k,l,ii,n,isp,count_f,count_s,count_nl,count_nl_global

  !!! /// init
   if(myid.eq.0) print*,'---scal initial nonlinear ---'

  !! try ps_data
  
   !ur = one
  !!  if(iglobstep.eq.2) stop
#if 0
     open(unit=155,file='ps_data_u.dat')
 !   write(unit1,*)'VARIABLES= ',' "X" ',' "Y" ',' "Z" ',' "UX" ', ' "UY" ',' "UZ" ' 
 !      write(unit1,*)'ZONE F=POINT, I=', mx-1,  ', J=', my-1, ', K=', mz-1
    do k =1, mz-1
    do j=1,my-1
       do i=1,mx-1
          read(155,*)ur(i,j,k,1),ur(i,j,k,2),ur(i,j,k,3)
       ! if(j.eq.10.and.k.eq.10)  write(*,*)ur(i,j,k,1),ur(i,j,k,2),ur(i,j,k,3)
       enddo
    enddo
    enddo
    close(155)
!    open(unit=155,file='data_u.dat')
!    write(155,*)'VARIABLES= ',' "X" ',' "Y" ',' "Z" ',' "UX" ', ' "UY" ',' "UZ" '  
!       write(155,*)'ZONE F=POINT, I=', mx-1,  ', J=', my-1, ', K=', mz-1
!    do k =1, mz-1
!    do j=1,my-1
!       do i=1,mx-1
!          write(155,*)i,j,k,ur(i,j,k,1),ur(i,j,k,2),ur(i,j,k,3)
       ! if(j.eq.10.and.k.eq.10)  write(*,*)ur(i,j,k,1),ur(i,j,k,2),ur(i,j,k,3)
!       enddo
!    enddo
!    enddo
!    close(155)

       
    e_u= zero
      ur(mx,:,:,:)= ur(1,:,:,:)
      ur(0,:,:,:)= ur(mx-1,:,:,:)
      ur(:,my,:,:)= ur(:,1,:,:)
      ur(:,0,:,:)= ur(:,my-1,:,:)
      ur(:,:,mz,:)= ur(:,:,1,:)
      ur(:,:,0,:)= ur(:,:,mz-1,:)
     
    do i=1,mx-1 
     do k =1, mz-1
    do j=1,my-1
     
          e_u(:)=e_u(:)+(ur(i,j,k,:)-urr(i,j,k,:))**2
      enddo
      enddo
        write(*,*) urr(i,10,10,1),ur(i,10,10,1)
     enddo
        write(*,*)sqrt(e_u(:))
       
  count_f = 0
  count_s = 0
   do k =1, mz-1
    do j=1,my-1
       do i=1,mx-1
    if(.not.fluid_atijk_scalar(i,j,k)) count_f=count_f+1
    if(.not.fluid_atijk(i,j,k)) count_s=count_s+1 
      
   enddo
   enddo
   enddo
  !!
      write(*,*)"scalar,fluid", count_f, count_s
#endif
#if 0     
    open(unit=155,file='ps_data_phi.dat')
 !   write(unit1,*)'VARIABLES= ',' "X" ',' "Y" ',' "Z" ',' "UX" ', ' "UY" ','
 !   "UZ" '
 !      write(unit1,*)'ZONE F=POINT, I=', mx-1,  ', J=', my-1, ', K=', mz-1
    do k =1, mz-1
    do j=1,my-1
       do i=1,mx-1
          read(155,*)phir(i,j,k,1)
       ! if(j.eq.10.and.k.eq.10)  write(*,*)ur(i,j,k,1),ur(i,j,k,2),ur(i,j,k,3)
       enddo
    enddo
    enddo
    close(155)
  !  open(unit=155,file='data_u.dat')
  !  write(155,*)'VARIABLES= ',' "X" ',' "Y" ',' "Z" ',' "UX" ', ' "UY" ',' "UZ"

  !     write(155,*)'ZONE F=POINT, I=', mx-1,  ', J=', my-1, ', K=', mz-1
  !  do k =1, mz-1
  !  do j=1,my-1
  !     do i=1,mx-1
  !        write(155,*)i,j,k,ur(i,j,k,1),ur(i,j,k,2),ur(i,j,k,3)
  !     ! if(j.eq.10.and.k.eq.10)  write(*,*)ur(i,j,k,1),ur(i,j,k,2),ur(i,j,k,3)
  !     enddo
  !  enddo
  !  enddo
  !  close(155)
  
      phir(mx,:,:,:)= phir(1,:,:,:)/heat_ratio(1)
      phir(0,:,:,:)= phir(mx-1,:,:,:)*heat_ratio(1)
      phir(:,my,:,:)= phir(:,1,:,:)
      phir(:,0,:,:)= phir(:,my-1,:,:)
      phir(:,:,mz,:)= phir(:,:,1,:)
      phir(:,:,0,:)= phir(:,:,mz-1,:)


#endif

    DO isp=1,nspmx
      ! DO k=1,mz-1
       !   DO j=1,my-1
      !       DO i=1,nx 
       onlphir(:,:,:,isp) =nlphir(:,:,:,isp)

        nlphir(:,:,:,isp)  =zero
      !    enddo
     !     enddo
     !     enddo
   enddo

     !!! important  
      br = zero  !!// right-hand side term

   !!// coef setting !! not include dt since the dt is change  then the coef of matrix also changes
	   
      cconv=one/dx
	 !!  cdiff_scal=gamma(isp)/dx/dx
         ! cdiff_scal_2=cdiff_scal*half
       !!   flag_pet_A_scal =.false.

         CALL form_nlphi
       ! CALL form_nlphi_ps (need to change)
    
	 
  count_nl_global =0
        do isp = 1, nspmx
          nlphimeanloc(isp) = zero
		     count_nl = 0
          do k = 1, nzp
             do j = 1, nyp
                do i = 1, nxp
                   nlphimeanloc(isp) = nlphimeanloc(isp)+ nlphir(i,j,k,isp)
                  !! if(.not.fluid_atijk_scalar(i,j,k))
                 !  if(i.eq.11.and.j.eq.10.and.k.eq.20) 
                 ! write(*,*)"br1",nlphir(i,j,k,isp),ur(i,j,k,1),onlphir(i,j,k,isp),phir(i,j,k,isp),phirold(i,j,k,isp),br(i,j,k,isp)


                  count_nl=count_nl+1
                end do
             end do
          end do
          GLOBAL_DOUBLE_SUM(nlphimeanloc(isp),nlphimean(isp),1,decomp_group)
		  GLOBAL_INT_SUM(count_nl,count_nl_global,1,decomp_group)
       end do
        nlphimean(:) = nlphimean(:)/count_nl_global
        if(I_AM_NODE_ZERO)Write(*,'(A25,10(2x,g12.5))')'NLPHIMEAN = ',( nlphimean(isp), isp = 1,nspmx)
      
       
       if(nbody.gt.0) CALL bcsetscal(rks,user)
	  
	    if(myid.eq. 0 ) write(*,*)'nl_scal done'


#if 0  
  do isp =1,nspmx
  do i= 1, nx
  do j =1,my-1
  do k = 1,mz-1
  if(i.eq.11.and.j.eq.10.and.k.eq.20)write(*,*)'what1',nlphir(i,j,k,isp),onlphir(i,j,k,isp)
  enddo
  enddo
  enddo
  enddo
#endif

  END SUBROUTINE nlphi_graduphi

   SUBROUTINE compute_phim_heat_ratio(output)
      
       IMPLICIT NONE
#include "include_petsc.h"
       
       LOGICAL, Intent(in) :: output
       INTEGER :: i,j,k, isp,ii,igb
              
       um = zero
       phim = zero
       countfl = 0
       countfl_plane = 0
       countfl_global = 0  
       um_global = zero
       phim_global = zero
   
      do isp = 1, nspmx
       do i = 1, nxp  !!!!note
          do k = 1, nzp
             do j = 1,nyp 
	              igb = i + xstart-1
			       ! GLOBAL_INDEX_X(igb)
                if(output)then
                   if(fluid_atijk_scalar(i,j,k))then
                     
                         phim(igb,isp) = phim(igb,isp) + (ur(i,j,k,1)+ur(i+1,j,k,1))/two*phir(i,j,k,isp)
                     
                      um(igb) = um(igb) + (ur(i,j,k,1)+ur(i+1,j,k,1))/two
                      ! countfl_plane(i) = countfl_plane(i) + 1
                       countfl(igb)= countfl(igb) + 1
                   else !!(if not)
                     
                   endif
                else    !! periodic only ???gaoi
                 !  do isp = 1, nspmx
                  !    phim(i,isp) = phim(i,isp) + ur(i,j,k,1)*phir(i,j,k,isp)
                  ! end do
                  ! um(i) = um(i) + ur(i,j,k,1)
                  ! countfl_plane(i) = countfl_plane(i) + 1
                endif
           
             end do
          end do
                !!  um(i) = um(i)/real(countfl_plane(i),prcn)
         
       end do  !! i
      enddo

           do isp = 1, nspmx
	     do i=1,mx-1
                GLOBAL_DOUBLE_SUM(phim(i,isp),phim_global(i,isp),1,decomp_group)

                GLOBAL_DOUBLE_SUM(um(i),um_global(i),1,decomp_group)
               
                GLOBAL_INT_SUM(countfl(i),countfl_global(i),1,decomp_group)

           !!  write(*,*)myid,'phim',i,igb,um(i),phim(i,isp)
               phim_global(i,isp) = phim_global(i,isp)/um_global(i) 
               um_global(i)= um_global(i)/ countfl_global(i)  !!*real(countfl_plane(i),prcn))
            !! if(myid.eq.0) write(*,*) 'phim_global',(i-ext_inlet)*dx,phim_global(i,isp),um_global(i)/countfl_global(i)
             end do
           enddo

     !!  if(I_AM_LAST_PROC)then
          do isp = 1, nspmx
             heat_ratio_new(isp) = one/phim_global(mx-1,isp)  !! phim(1,isp)/phim(nx+1,isp)!
          end do
     !!  end if
      ! BROADCAST_DOUBLE(heat_ratio_new(1),nspmx,END_PROC,decomp_group)

       if(I_AM_NODE_ZERO)then
          Write(*,*)'HEAT RATIO@N-1 : ', heat_ratio(:), 'HEAT_RATIO@N: ',&
                     & heat_ratio_new(:)
          !Write(*,*)'umo: ', um(mx), 'umi: ', um(1)
          Write(*,*) 'phimo: ', phim_global(mx-1,1), 'phimi: ', phim_global(1,1)
          !Write(*,*)'NLMEAN REQ:', (phim(mx)-phim(1))*um(1)
          !READ(*,*)
       end if
     END SUBROUTINE compute_phim_heat_ratio

END MODULE nlphi
    



